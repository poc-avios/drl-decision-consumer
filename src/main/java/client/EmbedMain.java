package client;

import java.util.Collection;

import org.drools.core.common.DefaultFactHandle;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avios.PartnersParameters;
import com.avios.Product;

/**
 * EmbedMain
 */
public class EmbedMain {

	public static void main(String[] args) {
		KieServices kieServices = KieServices.Factory.get();

		// create the container from classpath (alternative: lookup from maven repo)
		KieContainer kieContainer = kieServices.getKieClasspathContainer();
		
		// create a new session
		KieSession kieSession = kieContainer.newKieSession();
				
		partnersParameters(kieSession);
		
		// dispose
		kieSession.dispose();
	}

	private static void partnersParameters(KieSession kieSession) {
		
		System.out.println("########### partnersParameters #########");

		Product product = new Product();
		product.setPartner("BA");
		product.setProduct("Flight");
		product.setSubproduct("ET");
		
		// insert in the working memory
		kieSession.insert(product);
				
		// fire all rules
		kieSession.fireAllRules();

		Collection<? extends Object> objects = kieSession.getObjects();

		for (Object obj : objects) {
			if(obj instanceof PartnersParameters) {
				System.out.println(obj);
			}
		}
	}

}